SYMBOLS = {
    '}':'{', 
    ']':'[', 
    ')':'('
    }
SYMBOLS_L, SYMBOLS_R = (
    SYMBOLS.values(), 
    SYMBOLS.keys())

# https://www.zhihu.com/question/48739849
def check(word):
    arr = []
    for char in word:
        if char in SYMBOLS_L:
            # Left Line
            arr.append(char)
        elif char in SYMBOLS_R:
            # Right Line
            if arr and arr[-1] == SYMBOLS[char]:
                # Check mapped and pop  
                arr.pop()
            else:
                return False

    return not arr