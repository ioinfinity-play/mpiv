import numpy

def solution(A, K):
    return numpy.roll(A, K)

list_sample = []
right_rotate_index = 2
for i in range(0, 100):
    list_sample.append(i)

print(solution(list_sample, right_rotate_index))