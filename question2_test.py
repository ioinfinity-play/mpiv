import unittest

from question2 import check

'''
Testcase #1:
S = "{[()()]}"

Testcase #2:
S = "([)()]"

Testcase #3:
S = ")("

Testcase #4:
S = "{{{{"
'''

class InterviewTestCase(unittest.TestCase):

    def test_correct_check(self):
        self.assertTrue(check("{[()()]}"))
    
    def test_incorrect_1_check(self):
        self.assertFalse(check("([)()]"))
    
    def test_incorrect_2_check(self):
        self.assertFalse(check(")("))
        
    def test_incorrect_3_check(self):
        self.assertFalse(check("{{{{"))
    
    

if __name__ == '__main__':
    unittest.main()
